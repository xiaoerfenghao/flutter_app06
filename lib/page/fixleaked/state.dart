import 'package:fish_redux/fish_redux.dart';

class FixLeakedState implements Cloneable<FixLeakedState> {
  String content;
  @override
  FixLeakedState clone() {
//    return FixLeakedState();
    //级联语法给 clone 对象赋值
    return FixLeakedState()..content = this.content;
  }

}

FixLeakedState initState(Map<String, dynamic> args) {
//  return FixLeakedState();
  return FixLeakedState()..content = "MockLeakedPage";
}
