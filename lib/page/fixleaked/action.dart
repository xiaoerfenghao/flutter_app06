import 'package:fish_redux/fish_redux.dart';


//class Action {
//  const Action(this.type, {this.payload});
//  final Object type;
//  final dynamic payload;
//}
//TODO replace with your own action
enum FixLeakedAction {
  delay,
  modifyContent,
}

class FixLeakedActionCreator {
//  static Action onAction() {
//    return const Action(FixLeakedAction.action);
//  }
  ///创建 delay action，模拟耗时任务
  static Action delay() {
    return const Action(FixLeakedAction.delay);
  }

  ///创建修改 content 的 action
  static Action modifyContent(String content) {
    return Action(FixLeakedAction.delay, payload: content);
  }

}
